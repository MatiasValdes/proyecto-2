#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import gi
from gi.repository import Gtk
gi.require_version("Gtk", "3.0")


# Clase de informacion
class dlgAbout():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")
        # Extraemos el objeto desde glade
        self.dialogo = self.builder.get_object(object_name)
