# Visualizador de archivos pdb
El visualizador de archivos pdb es un visualizador que nos permite interactuar
con proteínas, esta aplicación nos permite observar información referida a 
valores filtrados como "ATOM", "HETATM", "ANISOU" y "OTHERS", además nos 
permite guardar el fragmento seleccionado con la extension .pdb también nos
permite ver la estructura 3D de las proteínas con un estilo de representación
cartoon

## Empezando
El presente proyecto consiste en la ejecución de una ventana principal la cual contiene cuatro opciones en botones: "abrir", "informacion", "Visualizar", "acerca de". Al seleccionar la opcion abrir se nos abrira una ventana en la cual el usuario deberá seleccionar la carpeta donde se encuentren los archivos pdb (debe estar dentro de la carpeta) si hay archivos saldrán listados en la ventana principal. Luego de tenerlo listado el usuario podrá elegir la proteína que desee, aquí se le presentarán dos opciones, si desea conocer información de la proteína debe presionar el botón "información" aquí se desplegará una ventana con los posibles filtros a conocer ("ATOM", "HETATM,"ANISOU","OTHERS") además se presenta un botón "guardar" en el cual el usuario luego de filtrar el archivo podrá guardar los datos que allí se le muestran en un nuevo archivo, la otra opcion que posee es la de visualizar al apretar el botón se abrirá una nueva ventana con la imagen en 3D de la proteína (si no se observa la imagen, deberá arrastrar el panel hacia abajo).
Finalmente encontrará un botón acerca de donde se ofrecerá información de los desarrolladores del programa.

### Pre-requisitos
* Sistema operativo Linux versión igual o superior a 18.04 (se recomienda 20.04)
* Python3
* Pymol
* Biopandas

### Instalación
1) Para ejecutar el programa debe asegurarse que tenga instalados los pre-requisitos
2) Si no posee instalado Pymol ésta se realiza a través del siguiente código: sudo apt-get install python3-pymol
3) Si no posee instalado Biopandas ésta se realiza a través del siguiente código: pip3 install biopandas
4) Asegurese de tener instalada la ultima versión de python, especialmente la versión 3.
5) Asegurese de que en su computadora se encuentre instalada la librería Gtk. Para esto realice el siguiente procedimiento:
* En consola teclee "python3" luego "import gi" si no genera ningún problema es que no tendrá problemas con aquellas librerías, en cambio, si generó algun tipo de problema ejecute el siguiente codigo: "sudo apt-get install python3-gi"

### Ejecutando las pruebas
Para ejecutar el código debe hallarse en el directorio donde se encuentre el archivo "main.py" desde terminal colocar: "python3 main.py"

### Construido con
* Ubuntu: sistema operativo
* Python: lenguaje de programación
* Gtk: librería que posee herramientas de interfaces gráficas
* Glade: herramienta para desarrollar interfaces gráficas, sencillas de manejar
* Atom: Editor de codigo
* Pymol: Herramienta que permite la visualizacion de moleculas
* Biopandas: libreria para trabajar con estructuras moleculares almacenadas en dataframes

## Versiones
# Versiones de herramientas
 * Ubuntu 20.04.01 LTS
 * Python 3.8.2
 * Atom 1.46.0
 * Glade 3.22.2
 * Gtk 3.24.18
 * Pymol 2.3.0
 * Biopandas 0.2.5

# Versiones de código
https://gitlab.com/MatiasValdes/proyecto-2

# Autores
* Matías Valdés: Desarrollo de código e interfaz, desarrollo de README
* Matías Vergara: Desarrollo de código e interfaz, cooperación de README

# Pagina Web
* https://buenabro.000webhostapp.com/

# Expresiones de Gratitud
* Ejemplos entregados en la plataforma de GitLab de Fabio Durán Verdugo: https://gitlab.com/fabioduran/programacion-avanzada-2020/-/tree/master/unidad1/gtk
Además de su constante esfuerzo por enseñarnos y a su amabilidad por resolvernos dudas en cualquier momento.
