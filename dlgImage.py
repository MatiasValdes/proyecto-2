#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
import pymol
import __main__
from gi.repository import Gtk
__main__.pymol_argv = ["pymol",  "-qc"]
pymol.finish_launching()
gi.require_version("Gtk", "3.0")


# Clase de la ventana de imagen
class dlgImage():
    def __init__(self, path="", name=""):
        # Traemos datos del main
        self.path = path
        self.name = name
        self.nombre = ""
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")
        # Construimos la ventana y el gtk.image
        self.foto = self.builder.get_object("imagen")
        self.proteina = self.builder.get_object("visualizacion")
        # Boton aceptar
        self.button_aceptar = self.builder.get_object("boton_aceptar")
        self.button_aceptar.connect("clicked", self.button_aceptar_clicked)

    # metodo para visualizar proteinas
    def visualizar(self):
        pdb_file = self.path
        pdb_name = self.name
        pymol.cmd.load(pdb_file, pdb_name)
        pymol.cmd.disable("all")
        pymol.cmd.enable(pdb_name)
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(pdb_name)
        nombre = ''.join([self.path,
                          ".",
                          "png"])
        pymol.cmd.png("%s" % (nombre))
        pymol.cmd.ray()
        # pymol.cmd.quit()
        # La ruta completa de la imagen de la proteina procesada
        self.nombre = nombre
        # Limpiamos la imagen y colocamos la nueva
        self.proteina.clear()
        self.proteina.set_from_file(self.nombre)

    # Metodo del boton aceptar que desruye la ventana de visualizacion
    def button_aceptar_clicked(self, btn=None):
        self.foto.destroy()
