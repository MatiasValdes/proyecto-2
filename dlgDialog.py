#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import gi
from gi.repository import Gtk
gi.require_version("Gtk", "3.0")


# Clase dialogo
class dlgDialog():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")
        # Extraemos la ventana de dialogo
        self.dialogo = self.builder.get_object(object_name)
        # Boton aceptar
        button_ok = self.dialogo.add_button(Gtk.STOCK_OK,
                                            Gtk.ResponseType.OK)
        button_ok.set_always_show_image(True)
        button_ok.connect("clicked", self.button_ok_clicked)

    # Destruimos la ventana de dialogo
    def button_ok_clicked(self, btn=None):
        self.dialogo.destroy()
