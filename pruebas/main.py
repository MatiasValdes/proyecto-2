#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
import pathlib
from dlg import dlgAbrir
from dlgAbout import dlgAbout

class Principal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")

        self.window = self.builder.get_object("ventana")
        self.window.set_title("VISUALIZADOR DE MOLECULAS")
        self.window.maximize()
        # self.window.resize(600, 400)
        self.window.connect("destroy", Gtk.main_quit)

        # botones
        boton_abrir = self.builder.get_object("boton_elegir")
        boton_abrir.connect("clicked", self.boton_abrir_clicked)
        boton_sobre = self.builder.get_object("boton_about")
        boton_sobre.connect("clicked", self.boton_sobre_clicked)
        #boton_cargar = self.builder.get_object("boton_cargar")
        #boton_cargar.connect("clicked", self.boton_cargar_clicked)

        # label
        self.label_ejemplo = self.builder.get_object("label_principal")

        # tree
        self.tree = self.builder.get_object("tree")
        self.tree_model = Gtk.ListStore(*(1 * [str]))
        self.tree.set_model(model=self.tree_model)
        # RenderText
        """ 
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Archivo(s) PDB",
                                    cell_renderer=cell,
                                    text=0)
        self.tree.append_column(column)
        """
        self.window.show_all()

    def boton_abrir_clicked(self, btn=None):
        dlg = dlgAbrir(object_name="dlgFileChooser")
        dlg.dialogo.run()
        self.label_ejemplo.set_text(dlg.ruta)
        self.treeview = Gtk.TreeView.new_with_model(self.language_filter)
        for i, column_title in enumerate("prote"):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            self.treeview.append_column(column)

        for i, x in dlg.archivo:
            cell = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(x, cell, text=i)
            self.tree.append_column(column)
            # contador = contador + 1
        dlg.dialogo.destroy()


    def boton_sobre_clicked(self, btn=None):
        dlg = dlgAbout(object_name="dlgAbout")
        dlg.dialogo.run()
        dlg.dialogo.destroy()

    #def boton_cargar_clicked(self, btn=None):



if __name__ == "__main__":
    Principal()
    Gtk.main()
