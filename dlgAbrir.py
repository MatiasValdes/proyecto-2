#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import gi
from gi.repository import Gtk
import os

gi.require_version('Gtk', '3.0')


class dlgAbrir():
    def __init__(self, object_name=""):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")

        self.dialogo = self.builder.get_object(object_name)
        # Botón aceptar.
        button_ok = self.dialogo.add_button(Gtk.STOCK_OK,
                                            Gtk.ResponseType.OK)
        button_ok.set_always_show_image(True)
        button_ok.connect("clicked", self.button_ok_clicked)

        # Botón cancelar.
        button_cancel = self.dialogo.add_button(Gtk.STOCK_CANCEL,
                                                Gtk.ResponseType.CANCEL)
        button_cancel.set_always_show_image(True)
        button_cancel.connect("clicked", self.button_cancel_clicked)

    def button_ok_clicked(self, btn=None):
        self.ruta = self.dialogo.get_current_folder()
        self.archivo = [i for i in os.listdir(self.ruta) if i.endswith('.pdb')]

    def button_cancel_clicked(self, btn=None):
        self.ruta = "0"
        self.archivo = ""
        self.dialogo.destroy()
