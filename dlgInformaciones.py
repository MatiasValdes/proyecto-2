#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
import os
from gi.repository import Gtk
from biopandas.pdb import PandasPdb
from dlgDialog import dlgDialog
gi.require_version("Gtk", "3.0")


# Clase de la ventana informacion
class dlgInformaciones():
    def __init__(self, path="", name=""):
        # Traemos datos del main
        self.name = name
        self.path = path
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")
        # Construimos la ventana y el label
        self.info = self.builder.get_object("informaciones")
        self.label = self.builder.get_object("info")
        # Boton guardar
        self.boton_save = self.builder.get_object("boton_guardar")
        self.boton_save.connect("clicked", self.boton_save_clicked)
        # Construimos un tree
        self.tree_info = self.builder.get_object("tree_info")
        self.tree_info.connect("row-activated", self.ver_info)
        self.model = Gtk.ListStore(*(1 * [str]))
        self.tree_info.set_model(model=self.model)
        # RenderText
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Datos",
                                    cell_renderer=cell,
                                    text=0)
        self.tree_info.append_column(column)
        # Añadimos filas a nuestro tree
        self.model.append(["INFO"])
        self.model.append(["ATOM"])
        self.model.append(["HETATM"])
        self.model.append(["ANISOU"])
        self.model.append(["OTHERS"])

    # Metodo para ver la informacion de la proteina
    def ver_info(self, nombre, path, column):
        model, it = self.tree_info.get_selection().get_selected()
        if model is None or it is None:
            return
        ppdb = PandasPdb()
        # Extraemos el pdb con la ruta anterior
        ppdb.read_pdb(self.path)
        principal = ("Raw PDB file contents:\n\n%s\n..." %
                     ppdb.pdb_text[:1000])
        # Colocamos una info principal al label
        self.label.set_text(principal)
        # Obtenemos el value del tree
        value = model.get_value(it, 0)
        # Sacamos los datos con ese value
        info_texto = ppdb.df[value]
        # Revisamos que no esté vacio el dataframe
        if info_texto.empty is True:
            self.label.set_text("No hay información disponible")
        else:
            self.label.set_text(info_texto.to_string())

    # Metodo de guardar al apretar el boton guardar
    def boton_save_clicked(self, btn=None):
        # Obtenemos el value que se quiere guardar
        model, it = self.tree_info.get_selection().get_selected()
        if model is None or it is None:
            return
        ppdb = PandasPdb()
        ppdb.read_pdb(self.path)
        value = model.get_value(it, 0)
        # Creamos la ventana de dialogo
        ventana = dlgDialog("guardar")
        # Creamos una nueva ruta para el archivo pdb con el value filtrado
        path_guardar = "".join([self.name, "_", value.upper(), ".pdb"])
        existencia = os.path.isfile(path_guardar)
        # Revisamos que no esté creado anteriormente
        if existencia is False:
            # Vemos que el data no esté vacío
            if ppdb.df[value].empty is False:
                ppdb.to_pdb(path=path_guardar,
                            records=[value],
                            gz=False,
                            append_newline=True)
                ventana.dialogo.set_property("text",
                                             "¡¡Información guardada!!")
                response = ventana.dialogo.run()
                # Si está vacio el dialogo nos advertirá
            else:
                ventana.dialogo.set_property("text",
                                             "No hay información para guardar")

                response = ventana.dialogo.run()
        # Si el archivo ya está guardado, la ventana nos los dirá
        else:
            ventana.dialogo.set_property("text",
                                         "El archivo que intenta guardar ya"
                                         " existe")
            response = ventana.dialogo.run()

        # Cuando aprete aceptar, se destruye la ventana de dialogo
        if response == Gtk.ResponseType.OK:
            ventana.dialogo.destroy()

    # No realizamos un atributo de la clase con las rutas porque podía
    # darse el caso de que se obtuviera info de una proteína pero que se
    # quisiera realizar otra proteína
