#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
from dlgAbrir import dlgAbrir
from dlgAbout import dlgAbout
from dlgInformaciones import dlgInformaciones
from dlgImage import dlgImage
from gi.repository import Gtk
gi.require_version("Gtk", "3.0")


# Clase principal
class Principal():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./interfaz/interfaz.ui")
        self.path = ""
        self.ruta_completa = ""
        self.name_pdb = ""
        # Ventana principal
        self.window = self.builder.get_object("ventana")
        self.window.set_title("VISUALIZADOR DE MOLECULAS")
        self.window.maximize()
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_icon_from_file("./icono1.png")

        # botones
        boton_abrir = self.builder.get_object("boton_elegir")
        boton_abrir.connect("clicked", self.boton_abrir_clicked)
        boton_sobre = self.builder.get_object("boton_about")
        boton_sobre.connect("clicked", self.boton_sobre_clicked)
        boton_informacion = self.builder.get_object("boton_informacion")
        boton_informacion.connect("clicked", self.boton_informacion_clicked)
        boton_visualizar = self.builder.get_object("boton_visualizar")
        boton_visualizar.connect("clicked", self.boton_visualizar_clicked)
        boton_visualizar.set_label("Visualizar")

        # label
        self.label_ejemplo = self.builder.get_object("label_principal")

        # tree
        self.tree = self.builder.get_object("tree")
        self.tree_model = Gtk.ListStore(*(1 * [str]))
        self.tree.set_model(model=self.tree_model)
        # RenderText
        cell = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn(title="Archivo(s) PDB",
                                    cell_renderer=cell,
                                    text=0)
        self.tree.append_column(column)

        self.window.show_all()

    # Metodo que abre la ventana de elegir carpeta
    def boton_abrir_clicked(self, btn=None):
        # Instanciamos el objeto
        dlg = dlgAbrir(object_name="dlgFileChooser")
        dlg.dialogo.run()
        text = "<b> Usted está en " + dlg.ruta + "</b>"
        # Indicamos en el label en que ruta estamos
        self.label_ejemplo.set_markup(text)
        self.path = dlg.ruta       # Vemos si existen archivos pdb
        if len(dlg.archivo) == 0:
            if self.path == "0":
                self.label_ejemplo.set_text("Presionó cancelar")
            else:
                self.label_ejemplo.set_text("En este directorio"
                                            "no existen .pdb")
        else:
            # Agregamos los datos a nuestro tree
            for i in dlg.archivo:
                self.tree_model.append([i])
        dlg.dialogo.destroy()

    # Metodo que abre la ventana de informacion de creadores
    def boton_sobre_clicked(self, btn=None):
        dlg = dlgAbout(object_name="dlgAbout")
        dlg.dialogo.run()
        dlg.dialogo.destroy()

    # Metodo que da informacion sobre la proteina
    def boton_informacion_clicked(self, btn=None):
        # Sacamos la posición del objeto seleccionado
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return
        self.name_pdb = model.get_value(it, 0)
        file_ = ''.join([self.path,
                         "/",
                         self.name_pdb])
        # Obtenemos la ruta completa del archivo
        self.ruta_completa = file_
        # Instanciamos el objeto de ventana
        dlg = dlgInformaciones(self.ruta_completa, self.name_pdb)
        # Añadimos atributos a la ventana de informaciones
        dlg.info.set_title(self.name_pdb)
        dlg.info.resize(600, 400)
        dlg.info.show_all()

    # Metodo que nos permite obtener una imagen de la proteina
    def boton_visualizar_clicked(self, btn=None):
        # Sacamos la posición del objeto seleccionado
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            return
        self.name_pdb = model.get_value(it, 0)
        file_ = ''.join([self.path,
                         "/",
                         self.name_pdb])
        # Obtenemos nuevamente la ruta
        self.ruta_completa = file_
        # Instanciamos el objeto
        imagen = dlgImage(self.ruta_completa, self.name_pdb)
        # Añadimos atributos a la ventana de visualizacion
        text = "IMAGEN DE LA PROTEINA " + self.name_pdb.upper()
        imagen.foto.set_title(text)
        imagen.foto.maximize()
        imagen.visualizar()
        imagen.foto.show_all()

        # No realizamos un atributo de la clase con las rutas porque podía
        # darse el caso de que se obtuviera info de una proteína pero que se
        # quisiera realizar otra proteína


if __name__ == "__main__":
    Principal()
    Gtk.main()
